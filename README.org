#+TITLE: Integrating PostGuard into Filesender
#+AUTHOR: Bryan Rinders, s1060340
#+OPTIONS: H:2 toc:nil num:t
#+LATEX_CLASS: beamerruhuisstyle
#+LATEX_CLASS_OPTIONS: [department=icis, showinstitute=true, showdate=true, slidenumbers=relative, handout, 14pt]
#+LATEX_HEADER_EXTRA: \def\firstassessor{Dr. Bram Westerbaan}
#+LATEX_HEADER_EXTRA: \def\secondassessor{Prof. Dr. Bart Jacobs}
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col)

#+STARTUP: beamer

* Notes                                                            :noexport:
The theme is [[https://github.com/naiaden/presentations/tree/master/ruhuisstijl][ruhuisstyle]]; its a little buggy.

* This is the first structural section
** Frame 1
*** Thanks to Eric Fraga                                           :B_block:
:PROPERTIES:
:BEAMER_COL: 0.48
:BEAMER_ENV: block
:END:
for the first viable Beamer setup in Org
*** Thanks to everyone else                                        :B_block:
:PROPERTIES:
:BEAMER_COL: 0.48
:BEAMER_ACT: <2->
:BEAMER_ENV: block
:END:
for contributing to the discussion
**** This will be formatted as a beamer note                       :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:
** Frame 2 (where we will not use columns)
*** Request                                                :B_structureenv:
:PROPERTIES:
:BEAMER_env: structureenv
:END:
Please test this stuff!
- item
- two

* A second section

** section 2 frame 1
writing some stuff

- a list
- item
