;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-latex-packages-alist . (("" "cleveref" nil) ;must go after hyperref
                                           ("" "xcolor" nil)))
              (org-latex-classes . (("beamerruhuisstyle"
                                     "\\documentclass[]{beamerruhuisstijl}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
"
                                     ("\\section{%s}"       . "\\section*{%s}")
                                     ("\\subsection{%s}"    . "\\subsection*{%s}")
                                     ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
                                    ("beamerruhuisstyle169"
                                     "\\documentclass[]{beamerruhuisstijl169}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
"
                                     ("\\section{%s}"       . "\\section*{%s}")
                                     ("\\subsection{%s}"    . "\\subsection*{%s}")
                                     ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))
              (org-latex-title-command . "
\\begin{frame}
  \\titlepage
  \\vspace{1.7cm}
  \\begin{block}
    \\par\\vspace{1cm}
    \\color{white}
    Supervisors:
    \\newline \\firstassessor %dr. Bram Westerbaan
    \\newline \\secondassessor %dr. Bart Jacobs
  \\end{block}
\\end{frame}
")
              (org-cite-export-processors . ((latex biblatex "ieee") (t basic)))
              (org-export-with-sub-superscripts . t)
              (org-export-with-toc . t)
              (org-export-with-todo-keywords . nil)
              (org-latex-inactive-timestamp-format . "\\texttt{%s}")
              (org-latex-active-timestamp-format   . "\\texttt{%s}")
              (org-export-date-timestamp-format . "%F")
              (org-export-with-section-numbers . t)
              (org-latex-reference-command . "\\cref{%s}") ;from the cleveref package
              (org-latex-bib-compiler . "biber")
              (org-latex-pdf-process . ("%latex -interaction nonstopmode -output-directory %o %f"
                                        "%bib %b"
                                        "%latex -interaction nonstopmode -output-directory %o %f"
                                        "%latex -interaction nonstopmode -output-directory %o %f")))))
